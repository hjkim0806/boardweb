package polymorphism;

public class LgTV implements TV {
	private Speaker speaker;
	
	public LgTV() {
		System.out.println("LgTV -- ��ü ����");
	}
	
	public LgTV(Speaker speaker) {
		System.out.println("LgTV(speaker) -- ��ü ����");
		this.speaker = speaker;
	}
	
	@Override
	public void powerOn() {
		System.out.println("LgTV -- TV�� �Ҵ�.");
	}

	@Override
	public void powerOff() {
		System.out.println("LgTV -- TV�� ����.");
	}

	@Override
	public void volumeUp() {
		speaker.volumeUp();
	}

	@Override
	public void volumeDown() {
		speaker.volumeDown();
	}

}
