package polymorphism;

import java.io.FileNotFoundException;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.GenericXmlApplicationContext;

public class TVUser {
	public static void main(String[] args) throws FileNotFoundException {
		// 1. Spring 컨테이너를 구동한다.
		AbstractApplicationContext factory = new GenericXmlApplicationContext("applicationContext.xml");
		
		// 2. Spring 컨테이너로부터 필요한 객체를 요청(Lookup)한다.
		TV tv = (TV) factory.getBean("samsungTV");
		tv.powerOn();
		tv.volumeUp();
		
		// 3. Spring 컨테이너를 종료한다.
		factory.close();
	}
}
