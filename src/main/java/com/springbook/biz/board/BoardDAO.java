package com.springbook.biz.board;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.springbook.biz.common.JDBCUtil;

@Repository("boardDAO")
public class BoardDAO{
	private Connection conn = null;
	private PreparedStatement psmt = null;
	private ResultSet rs = null;
	
	private final String Board_Insert = "insert into board(title, writer, content) values(?,?,?)";
	private final String Board_Update = "update board set title=?, content=?, where seq=?";
	private final String Board_Delete = "delete board where seq=?";
	private final String Board_Get = "select * from vo where seq=?";
	private final String Board_GetList = "select * from board order by seq desc";
	
	public void insertBoard(BoardVO vo) {
		System.out.println(" ===> JDBC로 insertBoard() 기능 처리 ");
		try {
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(Board_Insert);
			psmt.setString(1, vo.getTitle());
			psmt.setString(2, vo.getWriter());
			psmt.setString(3, vo.getContent());
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(psmt, conn);
		}
	}
	
	public void updateBoard(BoardVO vo) {
		System.out.println(" ===> JDBC로 updateBoard() 기능 처리 ");
		try {
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(Board_Update);
			psmt.setString(1, vo.getTitle());
			psmt.setString(2, vo.getContent());
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(psmt, conn);
		}
	}
	
	public void deleteBoard(int seq) {
		System.out.println(" ===> JDBC로 deleteBoard() 기능 처리 ");
		try {
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(Board_Delete);
			psmt.setInt(1, seq);
			psmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(psmt, conn);
		}
	}
	
	public BoardVO getBoard(int seq) {
		System.out.println(" ===> JDBC로 getBoard() 기능 처리 ");
		BoardVO board = null;
		try {
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(Board_Get);
			psmt.setInt(1, seq);
			if(rs.next()) {
				// BoardVO(int seq, String title, String writer, String content, Date regDate, int cnt) 
				board = new BoardVO(rs.getInt("SEQ"), rs.getString("title"), rs.getString("writer"), rs.getString("content"), rs.getDate("regDate"), rs.getInt("cnt"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, psmt, conn);
		}
		return board;
	}
	
	public List<BoardVO> getBoardList() {
		System.out.println(" ===> JDBC로 getBoardList() 기능 처리 ");
		List<BoardVO> boardList = new ArrayList<BoardVO>();
		try {
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(Board_GetList);
			rs = psmt.executeQuery();
			while(rs.next()) {
				BoardVO board = new BoardVO(rs.getInt("seq"), rs.getString("title"), rs.getString("writer"), rs.getString("content"), 
						rs.getDate("regDate"), rs.getInt("cnt"));
				boardList.add(board);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, psmt, conn);
		}
		return boardList;
	}
	
}
