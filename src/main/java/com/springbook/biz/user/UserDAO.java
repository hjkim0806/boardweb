package com.springbook.biz.user;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.springbook.biz.common.JDBCUtil;

public class UserDAO{
	private Connection conn = null;
	private PreparedStatement psmt = null;
	private ResultSet rs = null;
	
	private final String USER_GET = "select * from users where id=? and password = ?";
	
	public UserVO getUser(UserVO vo) {
		UserVO user = null;
		try {
			System.out.println(" ===> JDBC로 getUser() 기능 처리");
			conn = JDBCUtil.getConnection();
			psmt = conn.prepareStatement(USER_GET);
			psmt.setString(1, vo.getId());
			psmt.setString(2, vo.getPassword());
			rs = psmt.executeQuery();
			if(rs.next()) {
				user = new UserVO(rs.getString("id"), rs.getString("password"), rs.getString("name"), rs.getString("role"));
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			JDBCUtil.close(rs, psmt, conn);
		}
		return user;
	}
}
